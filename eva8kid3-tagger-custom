#!/bin/bash
source eva8kid3-tagger
# ---

# Read optional arguments and set their variables accordingly
# All optional arguments are switches that MUST begin with `--` and which MUST preceed the film's filename.
declare -A OPTIONS
OPTIONS=([RETAG]=n [V]=n)
while [ x$(echo $1|egrep '^-.+') != x ];do OPTIONS[$(echo $1|sed s/^-//|tr '[:lower:]' '[:upper:]')]=y;shift;done

# Discard all stdout from this sript if the `-v` switch wasn't given
if [ x${OPTIONS[V]} == xn ];then exec 1>/dev/null;fi

# List of temporary files being used
TMPFILES=

# Utility functions
cleanup(){ RET=0;for IDX in $(seq 0 $((${#TMPFILES[@]}-1)));do rm -rf ${TMPFILES[$IDX]};let RET=RET+$?;done;[ x$RET == x0 ];}
fatalerror(){ echo "${txtred}ERROR${txtreset}";cleanup;exit 1;}

# Kill self relay trap
trap "fatalerror" SIGTERM

# Core routines
FILEPATH="$1"
printf ${txtunderline};echo $(($(echo "$FILEPATH"|wc -c)+2))|awk '{for(i=1;i<$1;i++){printf" "}}';echo ${txtreset}
if [ ! -f "$FILEPATH" ];then kill $$;fi
echo "${txtunderline}> ${txtbold}$FILEPATH${txtreset}"
DIRECTORY=$(dirname "$FILEPATH")
FILENAME=$(basename "$FILEPATH"|awk -F. '{printf$1;for(i=2;i<NF;i++){printf"."$i}}')
VIDEOTYPE=$((printf "$FILEPATH"|awk -F. '$NF=="dvdtag"{print"DVD"}'&&echo STD)|head -n1|xargs printf)
if [ $VIDEOTYPE == DVD ];then INFOPATH="$FILEPATH";else INFOPATH="$DIRECTORY/$FILENAME.stdtag";fi
if [ $VIDEOTYPE == DVD ];then TAGPATH="$DIRECTORY/VIDEO_TS/VIDEO_TS.tag";else TAGPATH="$DIRECTORY/$FILENAME.tag";fi
printf "Checking for existing tag ... "
if [ -f "$TAGPATH" ];then printf "${txtyellow}FOUND${txtreset} (checking '-retag' switch ...) ";if [ x${OPTIONS[RETAG]} == xy ];then echo "${txtgreen}RETAGGING${txtreset}";else echo "${txtyellow}ABORTING${txtreset}";cleanup;exit 2;fi;else echo "${txtgreen}NONE FOUND${txtreset}";fi

# TALB - Title
printf "\t- ${txtcyan}Title${txtreset} ... "
TAG[TITL]=`awk -F' = ' '$1=="TITLE"{print$2}' "$INFOPATH"`
if [ x$? == x0 ];then echo "${txtgreen}OK${txtreset}";else kill $$;fi
# TIT2 - Description
printf "\t- ${txtcyan}Description${txtreset} ... "
TAG[DESC]=`awk -F' = ' '$1=="DESCRIPTION"{print$2}' "$INFOPATH"`
if [ x$? == x0 ];then echo "${txtgreen}OK${txtreset}";else kill $$;fi
# TLEN - Duration
printf "\t- ${txtcyan}Duration${txtreset} ... "
if [ $VIDEOTYPE == DVD ];then TAG[DURA]=$(lsdvd -vq "$DIRECTORY/VIDEO_TS/" 2>/dev/null|sed -n 's,Title: \([0-9]*\)\, Length: \([0-9]*\):\([0-9]*\):\([0-9]*\).\([0-9]*\).*,\1 \2 \3 \4 \5,p'|awk '{printf("%09s",$2*3600000+$3*60000+$4*1000+$5);print"/"$1"/tlen"}'|sort -rnt/ -k1.1,1.9|tr '\n' ' '|sed -n 's,^\([0-9]*\).*,echo "\0"|sed -e "s\,tlen\,\1\,g",p'|bash|tr ' ' '\n'|egrep .+|awk -F/ '{var=$1/$3*100;if(var>60){print$1"/"$2"/"$1/$3*100}}'|tail -n1|cut -d/ -f1|sed s/[0]*//;[ x$(($(printf ${PIPESTATUS[*]}|sed 's/ /+/g'))) == x0 ]);else TAG[DURA]=$(ffprobe -show_files "$FILEPATH" 2>/dev/null|sed -n "s,duration=\(.*\)\..*,\1,p"|xargs expr 1000 \*;[ x$(($(printf ${PIPESTATUS[*]}|sed 's/ /+/g'))) == x0 ]);fi
if [ x$? == x0 ];then echo "${txtgreen}OK${txtreset}";else kill $$;fi
# TYER - Year
printf "\t- ${txtcyan}Year${txtreset} ... "
TAG[YEAR]=`awk -F' = ' '$1=="YEAR"{print$2}' "$INFOPATH"`
if [ x$? == x0 ];then echo "${txtgreen}OK${txtreset}";else kill $$;fi
# TCON - Genre
printf "\t- ${txtcyan}Genre${txtreset} ... "
TAG[GENR]=`awk -F' = ' '$1=="GENRE"{print$2}' "$INFOPATH"`
if [ x$? == x0 ];then echo "${txtgreen}OK${txtreset}";else kill $$;fi
# TPE3 - Director
printf "\t- ${txtcyan}Director${txtreset} ... "
TAG[DIRE]=`awk -F' = ' '$1=="DIRECTOR"{print$2}' "$INFOPATH"`
if [ x$? == x0 ];then echo "${txtgreen}OK${txtreset}";else kill $$;fi
# TPE1 - Cast
printf "\t- ${txtcyan}Cast${txtreset} ... "
TAG[CAST]=`awk -F' = ' '$1=="CAST"{print$2}' "$INFOPATH"`
if [ x$? == x0 ];then echo "${txtgreen}OK${txtreset}";else kill $$;fi
# PRIV - Rating
printf "\t- ${txtcyan}Rating${txtreset} ... "
TAG[RATI]=`awk -F' = ' '$1=="RATING"{print$2}' "$INFOPATH"`
if [ x$? == x0 ];then echo "${txtgreen}OK${txtreset}";else kill $$;fi
# APIC - Cover
printf "\t- ${txtcyan}Cover${txtreset} ... "
FILEPATH_COVER="$DIRECTORY/$FILENAME.apic"
TMPAPIC=$(mktemp);TMPFILES[${#TMPFILES[@]}]=$TMPAPIC
if [ -f "$FILEPATH_COVER" ];then while ! cp "$FILEPATH_COVER" $TMPAPIC;do sleep 5;done;else kill $$;fi
TAG[APIC]="$FILEPATH_COVER"
echo "${txtgreen}OK${txtreset}"

TMPTAG=$(mktemp);TMPFILES[${#TMPFILES[@]}]=$TMPTAG
printf "Generating ID3 tag ... "
MakeTag $TMPTAG
echo "${txtgreen}OK${txtreset}"

while ! cp $TMPTAG "$TAGPATH";do sleep 5;done

printf "Cleaning up ... "
if cleanup;then echo "${txtgreen}OK${txtreset}";else echo -e "${txtyellow}FAILED${txtreset}\n${txtred}Unclean exit - please remove any temporary files that could've been left behind manually.${txtreset}";fi
exit 0
